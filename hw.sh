#!/bin/bash
#: Title        : hw
#: Date         : 11-12-2015
#: Author       : Francesco de Guytenaere
#: Version      : 1.0
#: Description  : print Hello, World!
#: Options      : none

printf "%s\n" "Hello, World!"
