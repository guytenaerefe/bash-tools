## Bash tools
This repository contains some of my public bash scripts that I use to enhance development, resource and servermanagement and general usability. 

For now it only includes the scripts that I have cleaned up and are fit to be public. 